# staticsite-controller
> ⚠
> This is me figuring out the [operator-sdk](https://sdk.operatorframework.io)
> and the depths of the k8s api, don't use!

Create simple static sites with a custom resource definition.

## Usage
Install CRD and controller to the cluster:
```bash
make install
make deploy
```

Create a yaml manifest for your static site:
```yaml
apiVersion: my.domain/v0
kind: StaticSite
metadata:
  name: sample
spec:
  content: |
    # Hello world
    hello
```

Apply manifest to cluster:
```bash
kubectl apply -f <file>
```

Access your site in the browser under `http://<cluster_fqdn>/<sitename>`.

