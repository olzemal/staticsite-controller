/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"

	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	"github.com/gomarkdown/markdown"

	mydomainv0 "gitlab.com/olzemal/staticsite-controller/api/v0"
)

const (
	staticsiteFinalizer = "my.domain/finalizer"
)

// StaticSiteReconciler reconciles a StaticSite object
type StaticSiteReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Log    logr.Logger
}

//+kubebuilder:rbac:groups=my.domain,resources=staticsites,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=my.domain,resources=staticsites/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=my.domain,resources=staticsites/finalizers,verbs=update

func (r *StaticSiteReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("staticsite", req.NamespacedName)
	log.Info("Reconciling StaticSite")

	staticsite := &mydomainv0.StaticSite{}
	err := r.Get(ctx, req.NamespacedName, staticsite)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Info("StaticSite resource not found. Ignoring...")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get StaticSite")
		return ctrl.Result{}, err
	}

	deploymentPresent := r.Client.Get(ctx,
		types.NamespacedName{
			Name:      staticsite.Name + "-deployment",
			Namespace: staticsite.Namespace,
		},
		&appsv1.Deployment{}) == nil

	servicePresent := r.Client.Get(ctx,
		types.NamespacedName{
			Name:      staticsite.Name + "-service",
			Namespace: staticsite.Namespace,
		},
		&apiv1.Service{}) == nil

	ingressPresent := r.Client.Get(ctx,
		types.NamespacedName{
			Name:      staticsite.Name + "-ingress",
			Namespace: staticsite.Namespace,
		},
		&networkingv1.Ingress{}) == nil

	nginxConfPresent := r.Client.Get(ctx,
		types.NamespacedName{
			Name:      "nginx-" + staticsite.Name + ".conf",
			Namespace: staticsite.Namespace,
		},
		&apiv1.ConfigMap{}) == nil

	contentConfPresent := r.Client.Get(ctx,
		types.NamespacedName{
			Name:      staticsite.Name + "-index.html",
			Namespace: staticsite.Namespace,
		},
		&apiv1.ConfigMap{}) == nil

	markedForDeletion := staticsite.GetDeletionTimestamp() != nil

	if markedForDeletion {
		if controllerutil.ContainsFinalizer(staticsite, staticsiteFinalizer) {
			if deploymentPresent {
				log.Info("Deleting Deployment")
				err = r.Client.Delete(ctx, &appsv1.Deployment{
					ObjectMeta: metav1.ObjectMeta{
						Name:      staticsite.Name + "-deployment",
						Namespace: staticsite.Namespace,
					},
				})
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			if servicePresent {
				log.Info("Deleting Service")
				err = r.Client.Delete(ctx, &apiv1.Service{
					ObjectMeta: metav1.ObjectMeta{
						Name:      staticsite.Name + "-service",
						Namespace: staticsite.Namespace,
					},
				})
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			if ingressPresent {
				log.Info("Deleting Ingress")
				err = r.Client.Delete(ctx, &networkingv1.Ingress{
					ObjectMeta: metav1.ObjectMeta{
						Name:      staticsite.Name + "-ingress",
						Namespace: staticsite.Namespace,
					},
				})
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			if nginxConfPresent {
				log.Info("Deleting nginx ConfigMap")
				err = r.Client.Delete(ctx, &apiv1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "nginx-" + staticsite.Name + ".conf",
						Namespace: staticsite.Namespace,
					},
				})
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			if contentConfPresent {
				log.Info("Deleting content ConfigMap")
				err = r.Client.Delete(ctx, &apiv1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name:      staticsite.Name + "-index.html",
						Namespace: staticsite.Namespace,
					},
				})
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			controllerutil.RemoveFinalizer(staticsite, staticsiteFinalizer)
			err = r.Update(ctx, staticsite)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, nil
	}

	// Add finalizer if not already present
	if !controllerutil.ContainsFinalizer(staticsite, staticsiteFinalizer) {
		controllerutil.AddFinalizer(staticsite, staticsiteFinalizer)
		err = r.Update(ctx, staticsite)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	// Add deployment if not already present
	if !deploymentPresent {
		log.Info("Creating deployment")
		err = r.CreateDeployment(ctx, staticsite)
		if err != nil {
			log.Error(err, "Failed to create deployment")
			return ctrl.Result{}, err
		}
	}

	// Add service if not already present
	if !servicePresent {
		log.Info("Creating service")
		err = r.CreateService(ctx, staticsite)
		if err != nil {
			log.Error(err, "Failed to create service")
			return ctrl.Result{}, err
		}
	}

	// Add ingress if not already present
	if !ingressPresent {
		log.Info("Creating ingress")
		err = r.CreateIngress(ctx, staticsite)
		if err != nil {
			log.Error(err, "Failed to create ingress")
			return ctrl.Result{}, err
		}
	}

	if !nginxConfPresent {
		log.Info("Creating nginx ConfigMap")
		err = r.CreateNginxConfigMap(ctx, staticsite)
		if err != nil {
			log.Error(err, "Failed to create nginx ConfigMap")
			return ctrl.Result{}, err
		}
	}

	if !contentConfPresent {
		log.Info("Creating content ConfigMap")
		err = r.CreateContentConfigMap(ctx, staticsite)
		if err != nil {
			log.Error(err, "Failed to create content ConfigMap")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *StaticSiteReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&mydomainv0.StaticSite{}).
		Complete(r)
}

// CreateDeployment creates a Deployment specified by the namespacedName
func (r *StaticSiteReconciler) CreateDeployment(ctx context.Context, staticsite *mydomainv0.StaticSite) error {
	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      staticsite.Name + "-deployment",
			Namespace: staticsite.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(3),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app": staticsite.Name,
				},
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app": staticsite.Name,
					},
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:  "web",
							Image: "nginx:1.12",
							Ports: []apiv1.ContainerPort{
								{
									Name:          "http",
									Protocol:      apiv1.ProtocolTCP,
									ContainerPort: 80,
								},
							},
							VolumeMounts: []apiv1.VolumeMount{
								{
									MountPath: "/usr/share/nginx/html/" + staticsite.Name + "/index.html",
									Name:      "nginx-content",
									SubPath:   "index.html",
								},
								{
									MountPath: "/etc/nginx/conf.d/",
									Name:      "nginx-config",
								},
							},
						},
					},
					Volumes: []apiv1.Volume{
						{
							Name: "nginx-content",
							VolumeSource: apiv1.VolumeSource{
								ConfigMap: &apiv1.ConfigMapVolumeSource{
									LocalObjectReference: apiv1.LocalObjectReference{
										Name: staticsite.Name + "-index.html",
									},
								},
							},
						},
						{
							Name: "nginx-config",
							VolumeSource: apiv1.VolumeSource{
								ConfigMap: &apiv1.ConfigMapVolumeSource{
									LocalObjectReference: apiv1.LocalObjectReference{
										Name: "nginx-" + staticsite.Name + ".conf",
									},
								},
							},
						},
					},
				},
			},
		},
	}
	return r.Client.Create(ctx, deployment)
}

// CreateService creates a Service specified by the namespacedName
func (r *StaticSiteReconciler) CreateService(ctx context.Context, staticsite *mydomainv0.StaticSite) error {
	service := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      staticsite.Name + "-service",
			Namespace: staticsite.Namespace,
		},
		Spec: apiv1.ServiceSpec{
			Selector: map[string]string{
				"app": staticsite.Name,
			},
			Type: apiv1.ServiceTypeClusterIP,
			Ports: []apiv1.ServicePort{
				{
					Name:     "http",
					Protocol: apiv1.ProtocolTCP,
					Port:     80,
				},
			},
		},
	}
	return r.Client.Create(ctx, service)
}

func (r *StaticSiteReconciler) CreateIngress(ctx context.Context, staticsite *mydomainv0.StaticSite) error {
	pathTypePrefix := networkingv1.PathType("Prefix")
	ingress := &networkingv1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      staticsite.Name + "-ingress",
			Namespace: staticsite.Namespace,
		},
		Spec: networkingv1.IngressSpec{
			Rules: []networkingv1.IngressRule{
				{
					IngressRuleValue: networkingv1.IngressRuleValue{
						HTTP: &networkingv1.HTTPIngressRuleValue{
							Paths: []networkingv1.HTTPIngressPath{
								{
									Path:     "/" + staticsite.Name,
									PathType: &pathTypePrefix,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: staticsite.Name + "-service",
											Port: networkingv1.ServiceBackendPort{
												Number: 80,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	return r.Client.Create(ctx, ingress)
}

func (r *StaticSiteReconciler) CreateNginxConfigMap(ctx context.Context, staticsite *mydomainv0.StaticSite) error {
	cfgmap := &apiv1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "nginx-" + staticsite.Name + ".conf",
			Namespace: staticsite.Namespace,
		},
		Data: map[string]string{
			staticsite.Name + ".conf": `
server {
	listen 80;
	root   /usr/share/nginx/html;
	index  index.html;
	server_name _;
}`,
		},
	}
	return r.Client.Create(ctx, cfgmap)
}

func (r *StaticSiteReconciler) CreateContentConfigMap(ctx context.Context, staticsite *mydomainv0.StaticSite) error {
	md := []byte(staticsite.Spec.Content)
	html := markdown.ToHTML(md, nil, nil)

	cfgmap := &apiv1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      staticsite.Name + "-index.html",
			Namespace: staticsite.Namespace,
		},
		Data: map[string]string{
			"index.html": string(html),
		},
	}
	return r.Client.Create(ctx, cfgmap)
}

func int32Ptr(i int32) *int32         { return &i }
func intStr(i int) intstr.IntOrString { return intstr.FromInt(i) }
